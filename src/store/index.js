import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

let counter = 1;
export const store = new Vuex.Store({
  state: {
    responses: [],
  },
  getters: {},
  mutations: {
    SET_DISTANCE_LOG: (state, payload) => {
      payload.idRecord = counter;
      state.responses.push(payload);
      counter ++
    }
  },
  actions: {
    GET_DISTANCE: async (context, payload) => {
      const { from, to } = payload;
      let data = {
        time: new Date().toLocaleString("ru"),
        direction: `${from} - ${to}`
      };
      try {
        const codes = await Promise.all([
          ymaps.geocode(from),
          ymaps.geocode(to)
        ]).then(values => {
          return values.map(item =>
            item.geoObjects.get(0).geometry.getCoordinates()
          );
        });
        const distance = await Math.round(
          ymaps.coordSystem.geo.getDistance(...codes) / 1000
        );
        data.distance = `${ distance } км`
      } catch (e) {
        data.distance = e.message;
      } finally {
        context.commit("SET_DISTANCE_LOG", data);
      }
    }
  }
});
